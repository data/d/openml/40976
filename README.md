# OpenML dataset: Bike

https://www.openml.org/d/40976

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Author: Gregory Gay, Tim Menzies, Misty Davies, Karen Gundy-Burlet  
Source: [Zenodo](https://zenodo.org/record/322475)  
Please cite: Misty Davies. (2009). bike [Data set]. Zenodo.  
DOI: http://doi.org/10.5281/zenodo.322475  

**Bike Database**  
This data contains the “bike” example from Automatically finding the control variables for complex system behavior, Gregory Gay, Tim Menzies, Misty Davies, Karen Gundy-Burlet, Automated Software Engineering May 2010.

The last two columns are derived from the others. The second last column is the noise (variance) on the power and should be minimized. The last column shows a cluster number for each row (and these clusters were generated via an unsupervised learning, working on all columns except the last two).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40976) of an [OpenML dataset](https://www.openml.org/d/40976). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40976/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40976/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40976/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

